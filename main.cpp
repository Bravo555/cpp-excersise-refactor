#include <iostream>
#include <cstdlib>
#include <time.h>

using namespace std;

int main()
{
    srand(time(NULL));

    int range;
    cout << "Podaj zakres przedzialu: ";
    cin >> range;
    int arrSize = 2 * range + 1;

	int sum = 0;
    for (int i = 0; i < arrSize; i++)
    {
        int randomizedValue = rand() % arrSize - range;
		sum += randomizedValue;
        cout << "[" << i << "]:" << randomizedValue << endl;
    }
	float average = sum / arrSize;

    cout << "Srednia wartosc wylosowanych liczb: " << average << endl;
}

